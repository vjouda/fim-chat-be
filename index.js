const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');

const jsonParser = bodyParser.json();

const app = express();
app.use(cors());

const users = [];
const messages = {};

const createIndex = (from, to) => `${Math.min(from, to)}:${Math.max(from, to)}`;

app.post('/users', jsonParser, ({body: userData}, res) => {
	userData.id = users.length + 1;
	users.push(userData);
	res.json(userData);
});

app.get('/users', (req, res) => {
	res.json(users);
});

app.post('/messages', jsonParser, ({body}, res) => {
	const {from, to} = body;
	const index = createIndex(from, to);
	const thread = messages[index];
	if(thread){
		thread.push[body];
	} else {
		messages[index] = [body];
	}
	res.sendStatus(200);
});

app.get('/messages/:from/:to', ({params: {from, to}}, res) => {
	res.json(messages[createIndex(from, to)]);
});

const port = process.env.PORT || 8080;
app.listen(port, () => {
	console.log('Running on ' + port);
});